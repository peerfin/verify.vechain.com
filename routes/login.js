var express = require('express');
const session = require('express-session');
const dbOperations = require('../common_modules/db-operations');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('login', { title: 'Login', login_error: "" });
});

router.post('/', function (req, res) {
  const email = req.body.email,
    password = req.body.password;   
  dbOperations.isValidateLogin(email, password)
    .then((user_id) => {
      if (user_id === 0) {
        res.render('login', { title: 'Login', login_error: "The username or password you entered is incorrect." });
      }
      else {
        if (!req.session.loggedInUser) {
          req.session.loggedInUserId = user_id;
          req.session.isActive = true;           
          req.session.save(err => {
            if (!err) {
              res.render('home', { title: 'Home' });
            }
          });
        }
      }
    }, (err) => {
      console.log(err);
    })
});

module.exports = router;