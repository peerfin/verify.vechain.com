var express = require('express');
const dbOperations = require('../common_modules/db-operations');
var router = express.Router();
/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('register', { title: 'Express',signup_error:"" });
});
router.post('/', function (req, res, next) {

  const fullname = req.body.fullname,
    email = req.body.email,
    password = req.body.password;

  dbOperations.registerUser(fullname, email, password)
    .then((isSuccess) => {
      if (isSuccess) {
        res.render('confirm', { title: 'Confirm Registration', email: email });
      }
      else {
        res.render('register', { title: 'User Registration',signup_error:"email already exists" });
        
      }
    }, (err) => {
      console.log(err);
    })
});


module.exports = router;