-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 03, 2019 at 05:10 AM
-- Server version: 5.7.25-0ubuntu0.16.04.2
-- PHP Version: 7.0.33-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `doptale`
--

-- --------------------------------------------------------


--
-- Table structure for table `t_users`
--
CREATE TABLE `t_users` (
  `id` bigint(20) NOT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` text COLLATE utf8_unicode_ci NOT NULL,  
  `is_verified` tinyint(4) NOT NULL DEFAULT '0',
  `is_active` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pfn_users`
--

INSERT INTO `t_users` (`id`, `fullname`, `email`, `password`, `is_verified`, `is_active`) VALUES
(1, 'Jomon', 'aneeshm.rem@gmail.com', '08c33e914c86e7a65e91cb2861034aab', 1, 1),
(3, 'Jomon', 'jomon.george23@gmail.com', '08c33e914c86e7a65e91cb2861034aab', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_user_wallet_informations`
--
CREATE TABLE `t_user_wallet_informations` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `private_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- -------------------------------------------------------- 

--
-- Table structure for table `t_notarized_files`
--
CREATE TABLE `t_notarized_files` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_created_date` datetime NOT NULL,
  `is_notarized` tinyint(4) NOT NULL DEFAULT '0',
  `notarized_filename` varchar(255) COLLATE utf8_unicode_ci NULL,
  `file_notarized_date` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Indexes for tables
--

--
-- Indexes for table `t_users`
--
ALTER TABLE `t_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);


--
-- Indexes for table `t_user_wallet_informations`
--
ALTER TABLE `t_user_wallet_informations`
  ADD PRIMARY KEY (`id`);


--
-- Indexes for table `t_notarized_files`
--
ALTER TABLE `t_notarized_files`
  ADD PRIMARY KEY (`id`);

-- --------------------------------------------------------


--
-- AUTO_INCREMENT for tables
--


--
-- AUTO_INCREMENT for table `t_users`
--
ALTER TABLE `t_users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;


--
-- AUTO_INCREMENT for table `t_user_wallet_informations`
--
ALTER TABLE `t_user_wallet_informations`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_notarized_files`
--
ALTER TABLE `t_notarized_files`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
