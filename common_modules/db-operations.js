const dbConn = require('./db-connect');

function registerUser(fullname, email, password) {
    return new Promise((resolve, reject) => {
        const sql = `INSERT INTO t_users (fullname,email, password, is_verified, is_active) VALUES ('${fullname}','${email}', '${password}', 0,0)`;

        isUserExists(email)
            .then((isSuccess) => {
                if (isSuccess) {
                    dbConn.query(sql, function (err, result) {
                        if (err) {
                            reject(err);
                        }
                        if (result) {
                            if (result.insertId){
                                resolve(true);
                            }
                            else{
                                resolve(false);
                            }                            
                        }
                    });
                }
                else{
                    resolve(false);
                }
            }, (err) => {
                reject(err);
            })
    })
}

function isValidateLogin(email, password) {
    return new Promise((resolve, reject) => {
        const sql = `SELECT * FROM t_users WHERE email='${email}' AND password='${password}'`;

        dbConn.query(sql, function (err, result) {
            if (err) {
                reject(err)
            }
            if (result[0]) {
                resolve(result[0].id)
            } else {
                resolve(0)
            }
        });
    });
}


function isUserExists(email) {
    return new Promise((resolve, reject) => {
        const sql_checkAlreadyExists = `SELECT * FROM t_users WHERE email='${email}'`;

        dbConn.query(sql_checkAlreadyExists, function (err, result) {
            if (err) {
                reject(err)
            }
            if (!result[0]) {
                resolve(true)
            } else {
                resolve(false)
            }
        });
    });
}

module.exports = { registerUser,isValidateLogin }