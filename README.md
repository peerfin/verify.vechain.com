# verify.vechain.com

Verify project using  Vechain Blockchain

Verify is a holistic blockchain notarization system. In this system, notarizations are not static one-time timestamps, but instead can now be dynamic, moving, changing, and updatable values on the blockchain. Companies or applications using and building upon this service can determine how their Verify container addresses, each with its own private key, are made and interact with the authority given by namespaces, the value, and status represented by digital assets sent to that account, and the information sent as memos. Furthermore, they can optionally customize the open-source Verify system to make an application convention framework to best utilize the above features.
The key innovation of the Verify blockchain notarization service is not to just make a one-off timestamp of a fingerprint of a document, but instead to also take that document and combine it with the user’s account information to make a special and unique private key just for that file. That file’s dedicated private key is used to make a hierarchical deterministic (HD) account, i.g., a Verify container address. The system then makes a fingerprint of the document by hashing its contents and then signing that hash with the private key of the user. That signed fingerprint is sent in a transaction to the HD account. This HD account now contains both file metadata and its fingerprint, so it can be said to be “colored” with that file.
 
Once a Verify container address is made, it not only contains proof-of-existence a document existed at a time, but it also can be updatable with memos and messages, can be brandable so that it is created as a part of a unique designated line of products making them authenticate, and furthermore using advanced multi-signing contracts can be transferable from person to person.
 
 
One can easily conclude from the texts above that Vechain’s Verify service is used for notarizing and checking the authenticity of files notarized on the Vechain blockchain. Its uses are various, but one big one is to register important documents that need a certificate of origin and content. Vechain happily provides these in a .zip file after a successful notarization, containing a .pdf of the certificate along with the original file with a hash in its filename and updated .nty file which is your notarization history.
It's easiest to learn from examples so I would like to start with one of many possible uses of the Vechain’s Verify service.
 
To have a document marked with the Notary Public seal, you'll need to do the following:
 
 
 
1. Two pieces of identification (government-issued photo ID and another one with at least your name)
2. Verbal agreement in front of the Notary that you understand and can attest to the facts of the document
3. Sign the document with the Notary as a witness
4. The Notary will then affix their stamp to the document
5. The document is now notarized

 
 
Bitcoin ushered in the breakthrough technology of the blockchain. The problem for many years was to establish trust between two parties without an independent, reputable third party present. This was a roadblock for many industries and there was always a need to have a centralized middleman, extracting fees for their service as a witness (think Uber, Airbnb, Notary services).
One of the core functionalities of the blockchain involves historical immutability -- the inability to change past records once it's been verified. This breakthrough solution in the digital space has immense value and it's beginning to propagate across many industries.
 
 
Verify is a more robust notarization service built on top of Vechain’s blockchain. Verify seeks to provide all of the functionalities inherent in traditional notary services, which includes verifiability, transferability, and updatability, by taking advantage of Vechain’s naming service, multi-signature accounts, messaging and blockchain assets. With Verify on Vechain, new 2.0 notarizations are possible that provide far more feature and functionality than anything before. Now notarizations are not just one-off timestamps, but instead become proof of notarizations that are updatable, brandable, contain third party memos and verifications, and can even be transferable. The result is an ever-changing notarization to reflect the ever-changing real world.
 
Features and Advantages:- 
 
Namespaces
 
Namespaces can play a major role in maintaining credibility and handling of new company documents. With the addition of a namespace and mosaics, the Verify system becomes even more robust as it allows a sort of watermark on the updates, which provide proof that this update came from a recognizable company. 
 
 
Document Industry
 
Certificates and fake diplomas are becoming more and more accessible on the market, putting each one of us at risk. The process to determine the validity of these documents is often inefficient and time-consuming. This discourages institutions and employers from controls. 
 
The solution has been designed to solve these two problems: diplomas and certificates are tagged with QR codes linked to the blockchain, a secure, immutable, and incorruptible database. 
 
Scan the QR code on the document, it’s possible to access the data registered on the blockchain by issuing institution, and get confirmation of its authenticity. 
 
The owner of these document (or student, for diplomas) is able to register as such on the blockchain account of the document. This way, the document can’t be counterfeited and the verification process is made simple and transparent. 
 
Art Industry
 
The project has the purpose of recording the provenance of every artwork as well as allowing the customers to claim their ownership and verify the authenticity. 
Every piece of art has a story behind: from the materials used, all the way through the process until its completion. And the story does not end when the item is purchased, as everything that happens to the time after it has been sold, writes a unique story that is worth telling. All these events define the essence of the item and enhance its value. 
 
To give the deserved importance to the provenance of the pieces of art had created a solution that provides proof of origin and authenticity for artists, galleries, collectors, and customers. 
 
Fashion and Jewelry Industry
 
 
The concept of recording the data of every item immutably in order to prevent counterfeit, and give access to proof of origin and proof of ownership. Furthermore, Verify enables brands to connect with their customer on a personal level improving customer engagement and gathering business insights. All these features define the essence of items and enhance their value. 
 
 
* Archiving sensitive data or documents which require immutability and should not be tampered with.
* Proof-of-ownership for an original document.
* You could notarize a certificate of ownership for your assets such as vehicle repairs, maintenance, accidents, permits, and ect, sent as a message to the account, where it could represent the current state of your asset with sequential updates. If you sell your asset, you can transfer ownership of notarization account to the new owner of the asset.
* Regarding digital media licenses, namespace and digital assets can originate from the notarization account for the license and these assets can be sent to others representing rights in the license.
* Companies with luxury goods can make a namespace account that only they can control. They can publish this name on their business profile.
* They can then make a blockchain notarization for each and every item using things like serial numbers, high definition scans, chemical makeup, and so on, to identify and register each item uniquely. Since each item is unique, and each is fingerprinted, and each blockchain notarization comes from a registered and recognized source, any competitor offering counterfeit products without an accompanying blockchain notarization will be easily identified.
* Making on-chain notarizations of contracts signed by multiple parties which can then later be updated or transferred if desired.
* Adding concert tickets to an account and verifying that it hasn't been used before re-selling on a marketplace.

